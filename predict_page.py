import streamlit as st
import pickle
import cv2
import numpy as np
import pandas as pd
import os
import random
import time
import math
import argparse
import xlsxwriter
import openpyxl
from ultralytics import YOLO
from tracker import Tracker
from io import StringIO
from io import BytesIO
from PIL import Image


model = YOLO("yolov8n.pt")
tracker = Tracker()
fpsi = 0
print(fpsi)
colors = [(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)) for j in range(30)]
nb_detection = 0
numberOfSpeedVehicleCaptered = 0
detection_threshold = 0.5
carInfo = {}
dictCar = {}

def to_excel(df):
    output = BytesIO()
    writer = pd.ExcelWriter(output, engine='xlsxwriter')
    df.to_excel(writer, index=False, sheet_name='Sheet1')
    workbook = writer.book
    worksheet = writer.sheets['Sheet1']
    format1 = workbook.add_format({'num_format': '0'}) 
    worksheet.set_column('A:A', None, format1)  
    writer.save()
    processed_data = output.getvalue()
    return processed_data

def estimateSpeed(location1, location2, time1, time2):
    # l waqet fet 0.5 sec wla 1 sec 
    # Location badlou bil centroide
    locationcentre1 = [int((int(location1[0])+int(location1[2]))/2),int((int(location1[1])+int(location1[3]))/2)]
    locationcentre2 = [int((int(location2[0])+int(location2[2]))/2),int((int(location2[1])+int(location2[3]))/2)]
    #d_pixels = math.sqrt(math.pow(location2[0] - location1[0], 2) + math.pow(location2[1] - location1[1], 2)) old
    d_pixels = math.sqrt(math.pow(locationcentre2[0] - locationcentre1[0], 2) + math.pow(locationcentre2[1] - locationcentre1[1], 2))
    #ppm = ((abs(location2[0]-location2[2]))/width)*2 old
    ppm = (abs(location2[0]-location2[2])+abs(location1[0]-location1[2]))/4
    d_meters = d_pixels / ppm
    min_speed_per_time = 3
    if (time2-time1)<min_speed_per_time:
        return None
    fps = 1.0/(time2-time1)
    timing = (time2-time1)
    #speed = d_meters * fpsi * fps * 3.6
    #speed = (d_meters  * fps * 3.6)/fpsi
    #speed = (d_meters  * fps * 3.6)/fpsi
    #speed = (d_meters * 3.6)*fps*fpsi/min_speed_per_time
    speed = (d_meters/timing) * 3.6 * 30
    #speed = d_meters * fps * 3.6 /min_speed_per_time
    return speed

def captureValue(id,speed,worksheet,posX):
    sp = "{:.2f}".format(speed)
    id = "{:.0f}".format(id)
    # Write some numbers, with row/column notation.
    #worksheet.write("A"+str(posX), 0, id)
    #worksheet.write("B"+str(posX), 1, sp)
    worksheet.write("A"+str(posX), str(id))
    worksheet.write("B"+str(posX), str(sp))
    worksheet.write("C"+str(posX), str(sp))
    #breakpoint()

def read_video_file(file_path,frame_holder,model,tracker,nb_detection,fpsi,numberOfSpeedVehicleCaptered):
    # Create an new Excel file and add a worksheet.
    # ecraser_xlsx_si_existe("resultCsv.xlsx")

    workbook = xlsxwriter.Workbook("resultCsv.xlsx")
    worksheet = workbook.add_worksheet()

    # Widen the first column to make the text clearer.
    worksheet.set_column("A1:A1", 7)
    worksheet.set_column("B1:B1", 10)
    worksheet.set_column("C1:C1", 15)

    # Add a bold format to use to highlight cells.
    bold = workbook.add_format({"bold": True})

    # Write some simple text.
    worksheet.write("A1", "ID", bold)

    # Text with formatting.
    worksheet.write("B1", "Speed", bold)
    worksheet.write("C1", "Vechicle Type", bold)
    posX=2
    # Insert an image.
    #worksheet.insert_image("B5", "logo.png")
    #workbook.close()
    speed = None
    cap = cv2.VideoCapture(file_path)
    # oldFrame for showing that the last frame is finished
    oldFrame = None
    video_out_path = os.path.join('.', 'output.mp4')
    ret1, frame1 = cap.read()
    height, width, channels = frame1.shape
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    cap_out = cv2.VideoWriter(video_out_path, fourcc, 30.0, (width, height))

    while cap.isOpened():
        ret, frame = cap.read()
        if not ret:
            if oldFrame is not None:
                frame_holder.image(oldFrame, channels="BGR",caption='Processing succesfully finished !')
            break
        start_time = time.time()
        # => treatframe()
        results = model(frame)
        for result in results:
            detections = []
            for r in result.boxes.data.tolist():
                x1, y1, x2, y2, score, class_id = r
                x1 = int(x1)
                x2 = int(x2)
                y1 = int(y1)
                y2 = int(y2)
                class_id = int(class_id)
                if score > detection_threshold:
                    detections.append([x1, y1, x2, y2, score])
                    nb_detection = nb_detection + 1

            # If there is detection
            if detections != []:
                tracker.update(frame, detections)
            
            if tracker.tracks is not None:
                for track in tracker.tracks:
                    bbox = track.bbox
                    x1, y1, x2, y2 = bbox
                    track_id = track.track_id

                    tracked_Info = carInfo.get(track_id)
                    if tracked_Info is not None:
                        # estimate speed
                        #carInfo[track_id] = [int(x1),int(y1),time.time()]
                        location1 = [carInfo[track_id][0],carInfo[track_id][1],carInfo[track_id][2],carInfo[track_id][3]]
                        location2 = [int(x1),int(y1),int(x2),int(y2)] 
                        #location1 = [carInfo[track_id][0],carInfo[track_id][1],carInfo[track_id][2],carInfo[track_id][3]]
                        #location2 = [int((int(x1)+int(x2))/2),int((int(1)+int(x2))/2)]
                        if fpsi != 0:
                            speed = estimateSpeed(location1,location2,carInfo[track_id][4],time.time())
                        if speed is not None:
                            cv2.putText(frame,str(int(speed)),(int(x1),int(y2)), font, 1,(255,255,255),1,cv2.LINE_AA)
                            carInfo[track_id] = [int(x1),int(y1),int(x2),int(y2),time.time()]
                            uniqueCar = dictCar.get(track_id)

                            if uniqueCar is None:
                                # to remake
                                captureValue(track_id,speed,worksheet,posX)
                                print("intereed")
                                posX = posX + 1 
                                dictCar[track_id] = 1
                                numberOfSpeedVehicleCaptered = numberOfSpeedVehicleCaptered + 1
                        # ecraser and save new value and draw it 
                        # carInfo[track_id] = [int(x1),int(y1),time.time()]
                    else:
                        carInfo[track_id] = [int(x1),int(y1),int(x2),int(y2),time.time()]
                    

                    cv2.rectangle(frame, (int(x1), int(y1)), (int(x2), int(y2)), (colors[track_id % len(colors)]), 3)
                    cv2.circle(frame, (int((int(x1)+int(x2))/2),int((int(y1)+int(y2))/2)), radius=0, color=(0, 0, 255), thickness=3)
                    font = cv2.FONT_HERSHEY_SIMPLEX
                    cv2.putText(frame,"ID:"+str(track_id),(int(x1),int(y1)), font, 1,(255,255,255),1,cv2.LINE_AA)

        end_time = time.time()	
        if not (end_time == start_time):
            fpsi = 1.0/(end_time - start_time)
        # Treating ends here 
        # Display the frame in Streamlit
        oldFrame = frame
        frame_holder.image(frame, channels="BGR",caption='Processing ...')
        cap_out.write(frame)
        # Sauvegarder le fichier Excel
        # workbook.save('resultCsv.xlsx')
        # Fermer le classeur
        workbook.close()
        
    cap.release()
    cap_out.release()
    cv2.destroyAllWindows()
    State = st.write(
    """
    Processing Finished successfully now you can download your result files !
    """
    )
    st.write("""#### Download your result files : """)

    workbook_path = 'resultCsv.xlsx'
    workbook = pd.read_excel(workbook_path)

    # Display the CSV workbook in Streamlit
    st.dataframe(workbook)
    # Add a download button for the CSV file
    #csv_bytes = workbook.to_csv(index=False).encode()  # Convert the workbook to CSV bytes
    """
    csv_bytes = workbook.to_csv(index=True).encode('utf-8')
    st.download_button(label='Download CSV', data=csv_bytes, file_name='workbook.csv', mime='text/csv')
    """
    df_xlsx = to_excel(workbook)
    st.download_button(label='📥 Download Results as an excel file',
                                data=df_xlsx ,
                                file_name= 'SpeedResults.xlsx', 
                                mime='application/octet-stream')
    #csv_bytes = b"iam byte content"
    #st.download_button(label='Downworkbookload CSV', data=csv_bytes, file_name='workbook.csv', mime='text/csv')
    """
    st.download_button(
   "Press to Download the csv file",
   csv,
   "csvFile.csv",
   "text/csv",
   key='download-csv'
    )
    """
    video_file = open('output.mp4', 'rb')
    video_bytes = video_file.read()
    st.download_button(
   "📹 Download the processed video",
   video_bytes,
   "result_video.mp4",
   "video/mp4",
   key='download-video'
    )

# Day-light // Night Predection --> / Automatically decide 

def show_predict_page():
    st.title("Estimate Speed")
    st.write("""### We will need your traffic video""")

    """
    Video = (
        "Close Vehicle-Camera Distance",
        "Mid-Range Vehicle-Camera Distance",
        "Far-CCTV Vehicle-Camera Distance",
    )
    """
    Video = (
        "Yes",
        "No",
    )
    videoType = st.selectbox("Radar Mode :", Video)
    #education = st.selectbox("Education Level", education)
    if videoType == "Yes":
        MaxSpeed = st.slider("Maximal Road Speed Value :", 30, 120, 120)
    """
    if videoType == "Mid-Range Vehicle-Camera Distance":
        ppm = st.slider("PPM(Pixel Per Meter) Value :", 1, 20, 9)
    if videoType == "Far-CCTV Vehicle-Camera Distance":
        ppm = st.slider("PPM(Pixel Per Meter) Value :", 1, 20, 15)
    """
    #MaxSpeed = st.slider("Maximal Road Speed Value :", 30, 120, 120)

    st.write("""#### Upload your video """)
    file = st.file_uploader("Upload a video file", type=["mp4", "avi"])
    frame_holder = st.empty()
    if file is not None:
        # Temporary workaround to save the file locally for OpenCV to read
        with open("temp.mp4", "wb") as f:
            f.write(file.read())
        #read_video_file("temp.mp4",frame_holder,model,tracker,nb_detection,fpsi,numberOfSpeedVehicleCaptered)
        read_video_file("temp.mp4",frame_holder,model,tracker,nb_detection,fpsi,numberOfSpeedVehicleCaptered)