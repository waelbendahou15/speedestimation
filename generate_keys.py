import pickle 
from pathlib import Path

import streamlit_authenticator as stauth

names = ["Wael BEN DAHOU", "Zoubir ATMANI"]
usernames = ["wbendahou", "zatmani"]
passwords = ["XXXX", "XXXX"]

hashed_passwords = stauth.Hasher(passwords).generate()
# Used bcrypt for password hashing with is secured
file_path = Path(__file__).parent / "hashed_pw.pkl"
with file_path.open("wb") as file:
    pickle.dump(hashed_passwords,file)