import streamlit as st
import pandas as pd
import matplotlib.pyplot as plt
from PIL import Image

"""
@st.cache
def load_data():
    df = pd.read_csv("survey_results_public.csv")
    df = df[["Country", "EdLevel", "YearsCodePro", "Employment", "ConvertedComp"]]
    df = df[df["ConvertedComp"].notnull()]
    df = df.dropna()
    df = df[df["Employment"] == "Employed full-time"]
    df = df.drop("Employment", axis=1)

    country_map = shorten_categories(df.Country.value_counts(), 400)
    df["Country"] = df["Country"].map(country_map)
    df = df[df["ConvertedComp"] <= 250000]
    df = df[df["ConvertedComp"] >= 10000]
    df = df[df["Country"] != "Other"]

    df["YearsCodePro"] = df["YearsCodePro"].apply(clean_experience)
    df["EdLevel"] = df["EdLevel"].apply(clean_education)
    df = df.rename({"ConvertedComp": "Salary"}, axis=1)
    return df

df = load_data()
"""
def show_explore_page():
    
    image = Image.open('download.png')
    st.image(image, width=650)
    #st.image(image)
    #st.title("Computer Vision - Speed Prediction")
    st.title("Vehicle Speed Estimation App")
    st.write(
        """
    ### Wael BEN DAHOU - Capgemini Engineering Intern (2023)
    """
    )

    #data = df["Country"].value_counts()

    #fig1, ax1 = plt.subplots()
    #ax1.pie(data, labels=data.index, autopct="%1.1f%%", shadow=True, startangle=90)
    #ax1.axis("equal")  # Equal aspect ratio ensures that pie is drawn as a circle.

    #st.write("""#### Number of Data from different countries""")

    #st.pyplot(fig1)
    """
    st.write(
        """
    #### Mean Salary Based On Country
    """
    )

    #data = df.groupby(["Country"])["Salary"].mean().sort_values(ascending=True)
    #st.bar_chart(data)

    st.write(
        """
    #### Mean Salary Based On Experience
    """
    )
    """
    #data = df.groupby(["YearsCodePro"])["Salary"].mean().sort_values(ascending=True)
    #st.line_chart(data)

