import matplotlib.pyplot as plt
 
# initializing the data
x = [50, 60, 70, 80, 90]
y = [6732, 4808, 2784, 996, 177]

# plotting the data
plt.plot(x, y)
 
# Adding the title
plt.title("Number of instances captured within the video and their precision")
 
# Adding the labels
plt.ylabel("Number of instances captured")
plt.xlabel("Precision rate in %")
plt.show()