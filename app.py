import streamlit as st
import pickle 
import streamlit_authenticator as stauth
from pathlib import Path 
from predict_page import show_predict_page
from explore_page import show_explore_page
from source_page import show_source_page

# --- User Authentification ---
names = ["Wael BEN DAHOU", "Zoubir ATMANI"]
usernames = ["wbendahou", "zatmani"]

# Used bcrypt for password hashing with is secured
file_path = Path(__file__).parent / "hashed_pw.pkl"
with file_path.open("rb") as file:
    hashed_passwords = pickle.load(file)

authenticator = stauth.Authenticate(
    names,
    usernames,
    hashed_passwords,
    "user_cookie",
    "cookie_key",
    cookie_expiry_days=30)

name, authentication_status, username = authenticator.login("Login to the speed predection app", "main")

if authentication_status == False:
    st.error("Username/password is incorrect")
if authentication_status == None :
    st.warning("Please enter your username and password")
if authentication_status == True:
    # page = st.sidebar.selectbox("Predict Speed Or Explore Solution", ("Predict Speed", "Explore Solution"))
    authenticator.logout("Logout","sidebar")
    st.sidebar.title(f"Welcome {name}")
    st.sidebar.title("What to do ?")
    page = st.sidebar.selectbox(
        "Choose the app mode",
        ("Show instruction", 
        "Estimate Speed", 
        "Show the source code"))

    if page == "Estimate Speed":
        show_predict_page()
    elif page == "Show instruction":
        show_explore_page()
    else:
        show_source_page()
